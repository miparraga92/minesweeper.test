import { Cell } from './models/cell';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    public matrix: Cell[][] = [];
    public mines: Cell[] = [];
    private dimension = 8;
    private numberOfMines = 10;
    private cellsAnalyzed: Cell[] = [];
    public gameOver = false;
    public youWin = false;
    private countVisibles = 0;
    public timeTracking = 0;
    private interval = null;
    private firstClick = false;

    ngOnInit(): void {
        this.stopTimeTracking();
        this.gameOver = false;
        this.youWin = false;
        this.countVisibles = 0;
        this.timeTracking = 0;
        this.interval = null;
        this.firstClick = false;
        this.createMatrix();
        this.generateMines();
        this.setNumbers();
    }

    private createMatrix() {
        const range = [...Array(this.dimension).keys()];
        range.forEach(row => {
            this.matrix[row] = [];
            range.forEach(col => {
                this.matrix[row][col] = new Cell(row, col);
            });
        });
    }

    private generateMines() {
        this.mines = [];
        [...Array(this.numberOfMines).keys()].forEach(_ => {
            let row;
            let col;
            do {
                row = this.getRandomInt(this.dimension);
                col = this.getRandomInt(this.dimension);
            } while (this.matrix[row][col].value === 'mine');

            this.matrix[row][col].value = 'mine';
            this.mines.push(this.matrix[row][col]);
        });
    }

    private getRandomInt(max: number) {
        return Math.floor(Math.random() * (max));
    }

    private setNumbers() {
        const range = [...Array(this.dimension).keys()];
        range.forEach(row => {
            range.forEach(col => {
                if (this.matrix[row][col].value === 'mine') {
                    return;
                }
                let countMines = 0;
                const adjacentIndexRow = this.getAdjacentIndex(row);
                const adjacentIndexCol = this.getAdjacentIndex(col);
                adjacentIndexRow.forEach(i => {
                    adjacentIndexCol.forEach(j => {
                        if ((i !== row || j !== col) && this.matrix[i][j].value === 'mine') {
                            countMines++;
                        }
                    });
                });
                this.matrix[row][col].value = countMines === 0 ? '' : String(countMines);
            });
        });
    }

    private getAdjacentIndex(index: number): number[] {
        const adjacentIndexs = [];
        if (index > 0) {
            adjacentIndexs.push(index - 1);
        }
        adjacentIndexs.push(index);
        if (index < this.dimension - 1) {
            adjacentIndexs.push(index + 1);
        }
        return adjacentIndexs;
    }

    public onClickCell(cell: Cell) {
        if (this.gameOver) {
            return;
        }
        if (cell.flag || cell.visible) {
            return;
        }
        cell.visible = true;
        this.countVisibles++;
        switch (cell.value) {
            case '':
                this.cellsAnalyzed = [];
                this.showAdjacents(cell);
                this.verifyVictory();
                if(!this.firstClick){
                    this.firstClick = true;
                    this.initTimeTracking();
                }
                break;
            case 'mine': this.showAllMines();
                this.gameOver = true;
                this.stopTimeTracking();
                break;
            default: this.verifyVictory();
                if(!this.firstClick){
                    this.firstClick = true;
                    this.initTimeTracking();
                }
                break;
        }
    }

    private showAllMines() {
        this.mines.forEach(cell => {
            cell.visible = true;
        })
    }

    private showAdjacents(cell: Cell) {
        this.cellsAnalyzed.push(cell);
        const row = cell.row;
        const col = cell.col;
        const adjacentIndexRow = this.getAdjacentIndex(row);
        const adjacentIndexCol = this.getAdjacentIndex(col);
        adjacentIndexRow.forEach(i => {
            adjacentIndexCol.forEach(j => {
                if (!this.matrix[i][j].flag) {
                    if (!this.matrix[i][j].visible) {
                        this.matrix[i][j].visible = true;
                        this.countVisibles++;
                    }
                    if (!this.cellsAnalyzed.includes(this.matrix[i][j]) && this.matrix[i][j].value === '') {
                        this.showAdjacents(this.matrix[i][j]);
                    }
                }
            });
        });
    }

    private verifyVictory() {
        if (this.countVisibles === (this.dimension * this.dimension) - this.numberOfMines) {
            this.youWin = true;
            this.gameOver = true;
            this.stopTimeTracking();
        }
    }

    public onRightClickCell(cell: Cell, event) {
        event.preventDefault();
        if (this.gameOver) {
            return;
        }
        if (cell.visible) {
            return;
        }
        cell.flag = !cell.flag;
    }

    private initTimeTracking() {
        this.timeTracking = 0;
        this.interval = setInterval(() => {
            this.timeTracking++;
        }, 1000);
    }

    private stopTimeTracking(){
        if(!!this.interval){
            clearInterval(this.interval);
        }
    }
}
