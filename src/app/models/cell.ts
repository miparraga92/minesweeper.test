export class Cell {
    row: number;
    col: number;
    visible: boolean = false;
    flag: boolean = false;
    value: string = '';

    constructor(_row: number, _col: number){
        this.row = _row;
        this.col = _col;
    }
}